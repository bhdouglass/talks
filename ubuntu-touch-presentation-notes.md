Ubuntu Touch Talk (October 2018)

## SLIDE Brief Overview

- Ubuntu Touch is a mobile OS for phones & tablets
- Alternative to Android and iOS
- Privacy focused
- Run by a community of volunteers
- Completely Open Source

## History

- SLIDE: 2012 rumors about ubuntu coming to phones
    - ubuntu for android
- SLIDE: 2013 Canonical introduced ubuntu for phones
- SLIDE: Ubuntu Edge
    - failed kickstarter
    - dual boot android
    - Goal $32 million
    - $12 million gathered (record breaking)
- SLIDE: Settled on suru design
- Launched a few phones with bq & meizu
- SLIDE: April 5th 2017: Canonical dropped ubuntu touch & unity
- SLIDE: UBports
    - existed before as a group of porters
    - Marius Gripsgard, founder
    - Picked up development of Ubuntu Touch as a whole
    - SLIDE: fairphone 2, oneplus one, nexus 5
- SLIDE: UBports Foundation
    - Registered non-profit
    - 1 full time developer and 1 part time developer
- SLIDE: UBports Installer
    - Making things easier for everyone
- SLIDE: Xenial release (16.04/OTA-4 & OTA-5)
    - Break from Canonical, now in unkown territory
    - Major milestone (previously on vivid, end of life a long time ago, security issues, no updates)

## SLIDE Why do we need ubuntu on the phone?

- Ubuntu Touch is actually Linux, Android is only kinda Linux
- Android is dependent heavily on Google and other vendors
    - Many parts of Android are slowly moving into the proprietary Google Play Services
    - Very difficult to use without Google Play
    - Vendors ship devices with Adware, Bloatware, Crapware, etc, etc
- iOS: Walled garden, their way or the highway
- Privacy by default
    - Not integrated by default with proprietary cloud services (can still just Google Calendar, etc)
    - No back doors
    - Defaults to privacy focused DuckDuckGo search engine
    - No Ads or phoning home features
- Freedom
    - Do whatever the heck you want with your phone
    - Keep older devices for the length of their life, not the length of their support by a vendor
    - Install packages (like run a webserver)
    - Developer friendly
- Open Source everywhere
    - 100% of apps and the OS
- Community Driven

## SLIDE: Convergence

- Use one device for everything
- Option to install traditional apps (Libertine)
- Phone apps adjust to larger screen size
- SLIDE: Seemlessly switch between phone and desktop
    - Plugin in display, keyboard, and/or mouse

## Technology

### SLIDE: Core

- Mir
    - Display server (alternative to x11)
- Unity8
    - Desktop environment focused on Convergence
- Android Container
    - Low level components of android, drivers etc
    - Eventually Halium
- Libhybris
    - Translates api/driver calls from ubuntu to android
- Qt (qml/c++)
- Click packages
    - Debian packages with confinment
    - Image based updates of the OS, confined clicks, no apt
    - OpenStore
    - Clickable

### SLIDE: Apps

- QML/Javascript
- C++
- Python
- Go
- HTML/Javascript

## SLIDE: Stats (As of October 2018)

- Users: ? (privacy by default & GDPR)
- Push Notifications: 100,000 / day
- Community Members: ~2,000
- Apps
    - 355 native
    - 379 Webapps

## Future

- SLIDE: Anbox
    - Android apk compatability
    - Compliment, not replace native apps
- SLIDE: Halium (soon tm)
    - Standardize hardware layer
    - Port device once, run every other phone linux (plasma mobile, sailfish os)
- SLIDE: Librem 5
    - no android layer
    - $2 million kickstarter
- Find more hardware partners
- SLIDE: UI toolkits (Cross platform apps)
    - Ubuntu Touch ui toolkit
    - QQC2 controls (available now ota-5)
    - Kirigami (available ota-5)

## Demo

- SLIDE: Showoff the phone and tablet

## SLIDE: How can this benefit our business?

